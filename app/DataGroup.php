<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGroup extends Model
{
    protected $table = "data_group";

    public function data() {
        return $this->hasMany(Data::class);
    }

    public function getDataCountAttribute() {
        return $this->data()->count();
    }
}
