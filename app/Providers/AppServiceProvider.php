<?php

namespace App\Providers;

use App\Lib\FileParser\CsvParser;
use App\Services\DataService;
use App\Services\ExportService;
use App\Services\FileParserService;
use App\Services\PropertiesService;
use App\Services\TableService;
use App\Services\TreeService;
use App\Services\TransformationService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(TableService::class);
        $this->app->singleton(TreeService::class);
        $this->app->singleton(PropertiesService::class, PropertiesService::class);
        $this->app->singleton(DataService::class);
        $this->app->singleton(ExportService::class);

        $this->app->singleton(FileParserService::class);
        $this->app->singleton(TransformationService::class);
    }

    public function boot(TableService $tableService, FileParserService $fileParserService)
    {
        Blade::component('components.form.form', 'form');
        Blade::component('components.form.input-hidden', 'inputhidden');
        Blade::component('components.form.input-text', 'inputtext');
        Blade::component('components.form.input-select', 'inputselect');
        Blade::component('components.form.input-textarea', 'inputtextarea');
        Blade::component('components.form.input-checkbox', 'inputcheckbox');
        Blade::component('components.form.submit', 'submit');

        Blade::component('components.table.table', 'table');
        Blade::component('components.table.row', 'tablerow');
        Blade::component('components.table.header-row', 'tableheaderrow');
        Blade::component('components.table.header', 'tableheader');

        Blade::component('components.tree.tree', 'tree');

        Blade::component('components.text.link', 'link');

        $tableService->addSchema('transformation', [
            [
                'name' => 'nazov',
                'align' => 'left'
            ], [
                'name' => 'pocet parametrov'
            ]
        ]);

        $tableService->addSchema('transformation_schema', [
            [
                'name' => 'S',
                'align' => 'left',
                'flex' => false
            ], [
                'name' => 'nazov',
                'align' => 'left',
                'flex' => false
            ], [
                'name' => 'popis'
            ], [
                'name' => 'povinny',
                'flex' => false
            ]
        ]);

        $tableService->addSchema('export', [
            [
                'name' => 'zmena na',
                'align' => 'left',
                'flex' => false
            ], [
                'name' => 'pouzite data'
            ], [
                'name' => 'subor',
            ], [
                'name' => 'stav',
                'flex' => false
            ], [
                'name' => 'funkcny od',
                'flex' => false
            ]
        ]);

        $tableService->addSchema('data_group', [
            [
                'name' => 'nazov',
                'align' => 'left'
            ], [
                'name' => 'pocet zaznamov'
            ]
        ]);

        $tableService->addSchema('export_binding', [
            [
                'name' => 'nazov',
                'align' => 'left'
            ], [
                'name' => 'zmenit na'
            ]
        ]);

        $fileParserService->add('csv', new CsvParser());
    }
}
