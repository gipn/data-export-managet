<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = "data";
    protected $_values = null;

    protected function getValues()
    {
        if ($this->_values == null) {
            $this->_values = \json_decode($this->value, true);
        }
        return $this->_values;
    }

    public function group()
    {
        return $this->belongsTo(DataGroup::class);
    }

    public function getValue($name, $default = false)
    {
        $values = $this->getValues();
        return $values[$name] ?? $default;
    }
}
