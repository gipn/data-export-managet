<?php

namespace App\Http\Controllers;

use App\Data;
use App\DataGroup;
use App\Export;
use App\ExportBinding;
use App\Services\ExportService;
use App\Services\TableService;
use App\Services\TreeService;
use App\Transformation;
use App\TransformationSchema;
use App\Lib\Tree\Tree;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GreenCape\Xml\Converter;

class ExportController extends Controller
{
    public function index(TableService $tableService)
    {
        $exports = Export::where('customer_id', 1)
            ->with(['transformation', 'file'])
            ->orderBy('activated_at', 'desc')
            ->take(30)
            ->skip(0)
            ->get();

        $table = $tableService->createWithSchema('export');

        foreach ($exports as $export) {
            $table->addRow([$export->transformation->name, $export->dataGroup->name, $export->file->name, $export->state, $export->activated_at], [
                'onClick' => route('export.show', ['id' => $export->id])
            ]);
        }

        return view('export.index', [
            'table' => $table
        ]);
    }

    public function create()
    {
        $transformations = Transformation::all()
            ->keyBy('id')
            ->map(function($item, $key) {
                return $item->name;
            })
            ->toArray();

        $dataGroups = DataGroup::where('customer_id', 1)
            ->get()
            ->keyBy('id')
            ->map(function($item, $key) {
                return $item->name;
            })
            ->toArray();

        return view('export.create', [
            'transformations' => $transformations,
            'dataGroups' => $dataGroups
        ]);
    }

    public function store(Request $request, ExportService $exportService)
    {
        $export = new Export();
        $export->customer_id = 1;
        $export->data_group_id = $request->input('data_group_id');
        $export->transformation_id = $request->input('transformation_id');

        $file = $exportService->createFile($request->input('filename'), $request->input('extension'));
        $file->save();

        $export->file_id = $file->id;
        $export->activated_at = Carbon::now();
        $export->state = Export::STATE_IDLE;
        $export->save();

        return redirect()->route('export.index');
    }

    public function show($id, TreeService $treeService)
    {
        $export = Export::with(['bindings.transformationSchema', 'bindings.properties'])
            ->find($id);
        $transformationSchema = TransformationSchema::where('transformation_id', $export->transformation_id)
            ->with('properties')
            ->get();

        $schemaTreeData = [];
        foreach ($transformationSchema as $s) {
            $schemaTreeData[] = [
                'id' => $s->id,
                'name' => $s->output,
                'parent_id' => $s->parent_id,
                'onClick' => route('export_binding.create', [
                    'exportId' => $export->id,
                    'transformation_schema_id' => $s->id
                ])
            ];
        }
        $schemaTree = $treeService->build($schemaTreeData);

        $bindings = [];
        foreach ($export->bindings as $binding) {
            $bindings[] = [
                'from' => $binding->transformationSchema->output,
                'to' => $binding->command,
                'onClick' => route('export_binding.edit', [
                    'exportId' => $export->id,
                    'id' => $binding->id
                ])
                ];
        }

        return view('export.show', [
            'export' => $export,
            'bindings' => $bindings,
            'schemaTree' => $schemaTree,
            'onCreate' => route('export_binding.create', ['exportId' => $export->id])
        ]);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function prepareExportToArray($export, ExportService $exportService)
    {
        $data = Data::where('data_group_id', $export->data_group_id)
            ->take(10)
            ->get()
            ->all();

        $tree = $exportService->generateTree($export);
        return $exportService->createExporter($tree)
            ->forItems($data)
            ->toArray();
    }

    public function run(Request $request, $id, ExportService $exportService)
    {
        $export = Export::where('id', $id)
            ->with(['transformation', 'transformation.transformationSchema', 'bindings', 'bindings.transformationSchema'])
            ->first();
        $result = $this->prepareExportToArray($export, $exportService);

        $xml = new Converter($result['root']);
        $exportService->store($export, $xml->__toString());
        return \redirect()->route('export.show', [
            'id' => $id
        ]);
    }

    public function preview(Request $request, $id, ExportService $exportService)
    {
        $export = Export::where('id', $id)
            ->with(['transformation', 'transformation.transformationSchema', 'bindings', 'bindings.transformationSchema'])
            ->first();

        $result = $this->prepareExportToArray($export, $exportService);

        $xml = new Converter($result['root'] ?? []);
        dd($xml->__toString());
    }
}
