<?php

namespace App\Http\Controllers;

use App\DataGroup;
use App\Properties;
use App\Services\PropertiesService;
use App\Services\TableService;
use Illuminate\Http\Request;

class DataGroupController extends Controller
{
    public function index(TableService $tableService)
    {
        $groups = DataGroup::where('customer_id', 1)
            ->orderBy('name', 'asc')
            ->take(30)
            ->skip(0)
            ->get();
        
        $table = $tableService->createWithSchema('data_group');
        foreach ($groups as $group) {
            $table->addRow([$group->name, $group->dataCount], [
                'onClick' => route('data_group.show', ['id' => $group->id])
            ]);
        }

        return view('data_group.index', [
            'table' => $table
        ]);
    }

    public function create()
    {
        return view('data_group.create');
    }

    public function store(Request $request)
    {
        $group = new DataGroup();
        $group->name = $request->name;
        $group->customer_id = 1;
        $group->save();

        return redirect()->route('data_group.index');
    }

    public function show($id, PropertiesService $propertiesService)
    {
        $group = DataGroup::find($id);
        $properties = $propertiesService->allMap('data_group', $id);
        return view('data_group.show', [
            'dataGroup' => $group,
            'properties' => $properties
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function properties(Request $request, $id, PropertiesService $propertiesService)
    {
        $propertyNamespace = 'data_group';
        foreach ($request->all() as $name => $value) {
            $property = $propertiesService->findOrCreate($propertyNamespace, $id, $name);
            $property->value = $value;
            $property->save();
        }
        
        return redirect()->route('data_group.show', ['id' => $id]);
    }
}
