<?php

namespace App\Http\Controllers;

use App\Export;
use App\ExportBinding;
use App\TransformationSchema;
use App\Services\PropertiesService;
use Illuminate\Http\Request;

class ExportBindingController extends Controller
{
    public function index()
    {
        //
    }

    private function getAvailableSchema($export, PropertiesService $propertiesService)
    {
        return TransformationSchema::where('transformation_id', $export->transformation_id)
        ->orderBy('output', 'asc')
        ->get()
        ->keyBy('id')
        ->filter(function ($item, $key) use ($propertiesService) {
            $property = $propertiesService->find('transformation_schema', $item->id, 'needsUserInput');
            if (empty($property)) {
                return false;
            }
            return $property->value;
        })
        ->map(function ($item, $key) {
            return $item->getName();
        });
    }

    public function create($exportId, PropertiesService $propertiesService, Request $request)
    {
        $export = Export::where('id', $exportId)->first();
        $schema = TransformationSchema::find($request->query('transformation_schema_id', 0));
        return view('export_binding.form', [
            'schema' => $schema,
            'values' => [
                'transformation_schema_id' => $request->query('transformation_schema_id', 0)
            ],
            'onSubmit' => route('export_binding.store', [
                'exportId' => $exportId
            ])
        ]);
    }

    public function store(Request $request, $exportId)
    {
        $binding = new ExportBinding();
        $binding->export_id = $exportId;
        $binding->transformation_schema_id = $request->transformation_schema_id;
        $binding->command = $request->command;
        $binding->save();

        return redirect()->route('export.show', [
            'id' => $exportId
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($exportId, $id, PropertiesService $propertiesService)
    {
        $export = Export::where('id', $exportId)->first();
        $binding = ExportBinding::where('id', $id)
            ->with('transformationSchema')
            ->first();
        
        return view('export_binding.edit', [
            'schema' => $binding->transformationSchema,
            'values' => [
                'binding_id' => $id,
                'command' => $binding->command
            ],
            'onSubmit' => route('export_binding.update', [
                'exportId' => $exportId,
                'id' => $id
            ])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $exportId, $id)
    {
        $exportBinding = ExportBinding::find($id);
        $exportBinding->command = $request->input('command', "");
        $exportBinding->save();

        return redirect()->route('export.show', [
            'id' => $exportId
        ]);
    }

    public function destroy($id)
    {
        //
    }
}
