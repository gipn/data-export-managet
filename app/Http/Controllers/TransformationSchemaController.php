<?php

namespace App\Http\Controllers;

use App\TransformationSchema;
use App\Services\TransformationService;
use Illuminate\Http\Request;

class TransformationSchemaController extends Controller
{
    public function index()
    {
        //
    }

    public function create(Request $request, $transformationId)
    {
        return view('transformation_schema.form', [
            'onSubmit' => route('transformation_schema.store', [
                'id' => $transformationId,
            ]),
            'values' => [
                'parent' => $request->query('parent', 0)
            ]
        ]);
    }

    public function store(Request $request, $transformationId, TransformationService $transformationService)
    {
        $schema = new TransformationSchema();
        $schema->transformation_id = $transformationId;
        $schema->output = $request->input('output');
        $schema->parent_id = $request->input('parent', 0);
        $schema->description = $request->input('description') ?? "";
        $schema->behavior = $request->input('behavior') ?? "";
        $schema->required = $request->input('required') ?? false;
        $schema->save();
        $transformationService->addToSchema($schema);

        return redirect()->route('transformation.show', ['id' => $schema->transformation_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($transformationId, $id)
    {
        $schema = TransformationSchema::find($id);
        return view('transformation_schema.form', [
            'onSubmit' => route('transformation_schema.update', [
                'transformationId' => $transformationId,
                'id' => $id
            ]),
            'values' => [
                'output' => $schema->output,
                'description' => $schema->description,
                'required' => $schema->required,
                'behavior' => $schema->behavior
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $transformationId, $id, TransformationService $transformationService)
    {
        $schema = TransformationSchema::find($id);
        $schema->output = $request->input('output');
        $schema->description = $request->input('description') ?? "";
        $schema->required = $request->input('required') ?? false;
        $schema->behavior = $request->input('behavior') ?? "";
        $schema->save();
        $transformationService->addToSchema($schema);

        return redirect()->route('transformation.show', ['id' => $schema->transformation_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
