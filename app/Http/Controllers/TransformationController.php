<?php

namespace App\Http\Controllers;

use App\Transformation;
use App\Services\TableService;
use App\Services\TreeService;
use App\Services\PropertiesService;
use App\Lib\Tree\Tree;
use Illuminate\Http\Request;

class TransformationController extends Controller
{
    public function index(TableService $tableService)
    {
        $transformations = Transformation::orderBy('name', 'asc')
            ->with('transformationSchema')
            ->skip(0)
            ->take(30)
            ->get();

        $table = $tableService->createWithSchema('transformation');
        foreach ($transformations as $transformation) {
            $table->addRow([$transformation->name, $transformation->schemaCount], [
                'onClick' => route('transformation.show', ['id' => $transformation->id])
            ]);
        }
        return view('transformation.index', [
            'table' => $table
        ]);
    }

    public function create()
    {
        return view('transformation.create');
    }

    public function store(Request $request)
    {
        $transformation = new Transformation();
        $transformation->name = $request->name;
        $transformation->save();
        return redirect()->route('transformation.show', ['id' => $transformation->id]);
    }

    public function show($id, TreeService $treeService)
    {
        $transformation = Transformation::where('id', $id)
            ->with(['transformationSchema' => function ($query) {
                return $query->orderBy('output', 'asc');
            }, 'properties'])
            ->first();

        $properties = $transformation->properties->keyBy('name');
        $treeData = [];
        foreach ($transformation->transformationSchema as $s) {
            $treeData[] = [
                'id' => $s->id,
                'name' => $s->output,
                'parent_id' => $s->parent_id,
                'onClick' => route('transformation_schema.edit', [
                    'transformationId' => $transformation->id,
                    'id' => $s->id
                ]),
                'onAdd' => route('transformation_schema.create', [
                    'transformationId' => $transformation->id,
                    'parent' => $s->id
                ]),
                'note' => $s->behavior
            ];
        }
        $tree = $treeService->build($treeData);

        return view('transformation.show', [
            'transformation' => $transformation,
            'properties' => $transformation->properties->keyBy('name'),
            'tree' => $tree,
            'onAdd' => route('transformation_schema.create', [
                'transformationId' => $transformation->id
            ])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function markRepeatAt($id, $schemaId, PropertiesService $propertiesService)
    {
        $property = $propertiesService->findOrCreate('transformation', $id, 'repeatSchemaId');
        $property->value = $schemaId;
        $property->save();

        return redirect()->route('transformation.show', [
            'id' => $id
        ]);
    }
}
