<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller 
{
    public function download($publicId)
    {
        $file = File::where('public_id', $publicId)->first();
        return Storage::download($file->path, $file->name);
    }
}