<?php

namespace App\Http\Controllers;

use App\Data;
use App\DataGroup;
use App\Services\DataService;
use App\Services\FileParserService;
use App\Services\PropertiesService;
use Illuminate\Http\Request;

class DataImportController extends Controller
{
    public function clear($id, DataService $dataService)
    {
        $dataService->clear($id);
        return redirect()->route('data_group.show', ['id' => $id]);
    }

    public function set($id, PropertiesService $propertiesService, DataService $dataService, FileParserService $fileParserService)
    {
        $dataService->clear($id);
        $this->add($id, $propertiesService, $dataService, $fileParserService);
        return redirect()->route('data_group.show', ['id' => $id]);
    }

    public function add($id, PropertiesService $propertiesService, DataService $dataService, FileParserService $fileParserService)
    {
        $properties = $propertiesService->allMap('data_group', $id);
        $csvParser = $fileParserService->get('csv');
        $csv = $csvParser->fromFile($properties['url']);
        $dataService->add($id, $csv);
        return redirect()->route('data_group.show', ['id' => $id]);
    }
}
