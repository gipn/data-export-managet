<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertiesModel extends Model 
{
    protected $propertyNamespace = false;
    private $propertiesByKey = false;

    private function getPropertiesByKey()
    {
        if ($this->propertiesByKey === false) {
            $this->propertiesByKey = $this->properties->keyBy('name');
        }
        return $this->propertiesByKey;
    }

    public function properties()
    {
        $namespace = $this->table;
        if (!empty($this->propertyNamespace)) {
            $namespace = $this->propertyNamespace;
        }
        return $this->hasMany(Properties::class, 'foreign_id')->where('namespace', $namespace);
    }

    public function hasProperty($name)
    {
        return $this->getProperty($name) !== null;
    }

    public function getPropertyValue($name, $default = false)
    {
        $properties = $this->getPropertiesByKey();
        return $properties[$name]->value ?? $default;
    }

    public function getProperty($name)
    {
        $properties = $this->getPropertiesByKey();
        return $properties[$name] ?? null;
    }
}