<?php

namespace App\Services;

use App\TransformationSchema;
use App\File;
use App\Export;
use Illuminate\Support\Facades\Storage;
use App\Lib\Transformation\Prototype;
use App\Lib\Tree\Tree;
use App\Lib\Tree\IncompleteTree;
use App\Lib\Export\ExportTree;
use App\Services\TreeService;

class ExportService
{
    /** @var TreeService */
    protected $treeService;

    public function __construct(TreeService $treeService)
    {
        $this->treeService = $treeService;
    }

    public function generateTree(Export $export)
    {
        $treeData = [];
        $schemas = collect($export->transformation->transformationSchema)
            ->keyBy('id')
            ->all();

        // foreach ($export->transformation->transformationSchema as $schema) {
        //     $treeData[$schema->id] = [
        //         'id' => $schema->id,
        //         'parent_id' => $schema->parent_id,
        //         'name' => $schema->output,
        //         'behavior' => $schema->behavior
        //     ];
        // }

        $incompleteTree = $this->treeService->createIncomplete();
        foreach ($export->bindings as $binding) {
            $schema = $binding->transformationSchema;
            $incompleteTree->add($this->treeService->create([
                'id' => $schema->id . "_" . $binding->id,
                'parent_id' => $schema->parent_id,
                'name' => $schema->output,
                'behavior' => $schema->behavior,
                'command' => $binding->command
            ]));
        }
        while(($node = $incompleteTree->getMissing()) != null) {
            if (empty($schemas[$node->get('parent_id')])) {
                continue;
            }
            $schema = $schemas[$node->get('parent_id')];
            $incompleteTree->add($this->treeService->create([
                'id' => $schema->id,
                'parent_id' => $schema->parent_id,
                'name' => $schema->output,
                'behavior' => $schema->behavior
            ]));
        }
        $tree = $incompleteTree->getRoot();
        return $tree;
    }

    public function createExporter(Tree $tree)
    {
        return new ExportTree($tree);
    }

    public function store($export, $content)
    {
        Storage::put($export->file->path, $content);
        $export->file->size = Storage::size($export->file->path);
        $export->file->save();
    }

    public function createFile($name, $extension)
    {
        $file = new File();
        $file->public_id = str_random(32);
        $file->name = $name;
        $file->extension = $extension;
        $file->size = 0;
        $file->path = "exports/" . (time() % 1000) . "/" . $file->public_id . $file->extension;
        if (!ends_with($file->name, $file->extension)) {
            $file->name .= $file->extension;
        }
        return $file;
    }
}