<?php

namespace App\Services;

use App\TransformationSchema;
use App\Services\PropertiesService;

class TransformationService
{
    protected $propertiesService;

    public function __construct(PropertiesService $propertiesService)
    {
        $this->propertiesService = $propertiesService;
    }

    public function addToSchema($schema)
    {
        if ($schema->hasParent()) {
            $parentProperty = $this->propertiesService->findOrCreate('transformation_schema', $schema->parent_id, 'needsUserInput');
            $parentProperty->value = false;
            $parentProperty->save();
        }
        $property = $this->propertiesService->findOrCreate('transformation_schema', $schema->id, 'needsUserInput');
        $property->value = true;
        $property->save();
    }
}