<?php

namespace App\Services;

use App\Lib\Tree\Tree;
use App\Lib\Tree\IncompleteTree;

class TreeService
{
    public function build($data, $childKey = 'parent_id', $parentKey = 'id')
    {
        $root = $this->createRoot();
        $this->fill($root, $data, $childKey, $parentKey);
        return $root;
    }

    public function fill($node, $data, $childKey, $parentKey)
    {
        $children = $node->createChildrenWhere($data, $childKey, $node->getProperty($parentKey));
        foreach ($children as $child) {
            $this->fill($child, $data, $childKey, $parentKey);
        }
    }

    public function create($data)
    {
        return new Tree($data);
    }

    public function createIncomplete()
    {
        return new IncompleteTree($this->createRoot());
    }

    private function createRoot()
    {
        return $this->create([
            'id' => 0,
            'name' => 'root'
        ]);
    }
}