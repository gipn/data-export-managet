<?php

namespace App\Services;

use App\Data;

class DataService 
{
    public function add($groupId, $data)
    {
        foreach ($data as $values) {
            $data = Data::where('data_group_id', $groupId)
                ->where('customer_internal_id', $values['id'])
                ->first();
            if (empty($data)) {
                $data = new Data();
                $data->customer_internal_id = $values['id'];
                $data->data_group_id = $groupId;
            }
            $data->value = json_encode($values);
            $data->save();
        }
    }

    public function clear($groupId)
    {
        Data::where('data_group_id', $groupId)->delete();
    }

    public function set($groupId, $data)
    {
        $this->clear();
        $this->add($groupId, $data);
    }
}