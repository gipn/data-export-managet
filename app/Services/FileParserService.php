<?php

namespace App\Services;

class FileParserService
{
    protected $data = [];

    public function add($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function get($key)
    {
        return $this->data[$key];
    }
}
