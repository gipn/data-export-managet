<?php

namespace App\Services;

use App\Properties;

class PropertiesService 
{
    public function all($namespace, $foreignId)
    {
        return Properties::where('namespace', $namespace)
            ->where('foreign_id', $foreignId)
            ->get();
    }

    public function allMap($namespace, $foreignId)
    {
        return $this->all($namespace, $foreignId)
            ->keyBy('name')
            ->map(function ($item, $key) {
                return $item->value;
            })
            ->toArray();
    }

    public function find($namespace, $foreignId, $name)
    {
        return Properties::where('namespace', $namespace)
            ->where('foreign_id', $foreignId)
            ->where('name', $name)
            ->first();
    }

    public function findOrCreate($namespace, $foreignId, $name)
    {
        $property = $this->find($namespace, $foreignId, $name);
        if (!$property) {
            $property = new Properties();
            $property->namespace = $namespace;
            $property->foreign_id = $foreignId;
            $property->name = $name;
        }
        return $property;
    }
}