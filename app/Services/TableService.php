<?php

namespace App\Services;

use App\Lib\Table\Table;

class TableService {
    protected $schemas = [];

    public function create(): Table {
        return new Table();
    }

    public function createWithSchema($schemaName): Table {
        return new Table($this->schemas[$schemaName]);
    }

    public function addSchema($name, $schema) {
        $this->schemas[$name] = $schema;
    }
}