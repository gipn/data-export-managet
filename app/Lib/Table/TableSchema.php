<?php

namespace App\Lib\Table;

class TableSchema {
    protected $schema;

    public function __construct($schema = []) 
    {
        $this->set($schema);
    }

    public function set($schema) 
    {
        $this->schema = $schema;
    }

    public function add($column)
    {
        $this->schema[] = $column;
    }

    public function get($i) 
    {
        return $this->schema[$i];
    }

    public function all() 
    {
        return $this->schema;
    }
}