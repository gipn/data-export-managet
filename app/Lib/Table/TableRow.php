<?php

namespace App\Lib\Table;

class TableRow {
    protected $properties;
    protected $values;
    protected $schema;

    public function __construct($schema, $values, $properties = []) {
        $this->schema = $schema;
        $this->values = $values;
        $this->properties = $properties;
    }

    public function hasProperty($property) {
        return isset($this->properties[$property]);
    }

    public function getProperty($property, $default = null) {
        return $this->properties[$property] ?? $defaults;
    }

    public function getValues() {
        return $this->values;
    }

    public function getValue($column) {
        return $this->values[$column];
    }

    public function getCells() {
        $cells = [];
        $columnsCount = count($this->getValues());
        for ($i = 0; $i < $columnsCount; $i++) {
            $cells[] = $this->getCell($i);
        }
        return $cells;
    }

    public function getCell($columnIndex) {
        return [
            'value' => $this->getValue($columnIndex),
            'schema' => $this->schema->get($columnIndex)
        ];
    }
}
