<?php

namespace App\Lib\Table;

class Table {
    protected $rows = [];
    protected $schema;

    public function __construct($schema = []) 
    {
        $this->schema = new TableSchema($schema);
    }

    public function getSchema() 
    {
        return $this->schema->all();
    }

    public function setSchema($schema): Table 
    {
        $this->schema->set($schema);
        return $this;
    }

    public function editable(): Table
    {
        $this->schema->add([
            'name' => '',
            'align' => 'right',
            'flex' => false
        ]);
        return $this;
    }

    public function addRow($values, $properties = []): Table 
    {
        $this->rows[] = new TableRow($this->schema, $values, $properties);
        return $this;
    }

    public function getRows() 
    {
        return $this->rows;
    }

    public function countRows() 
    {
        return count($this->rows);
    }
}