<?php

namespace App\Lib\Tree;

class IncompleteTree
{
    protected $limbo = [];
    protected $map = [];
    /** @var Tree */
    protected $root;

    public function __construct(Tree $root)
    {
        $this->root = $root;
        $this->map[$this->root->get('id')] = $this->root;
    }

    public function add(Tree $node)
    {
        $id = $node->get('id');
        $this->map[$id] = $node;
        if (isset($this->map[$node->get('parent_id')])) {
            $this->map[$node->get('parent_id')]->addChild($node);
        } else {
            $this->limbo[] = $node;
        }
        foreach ($this->limbo as $key => $value) {
            if ($value->get('parent_id') != $id) {
                continue;
            }
            $node->addChild($value);
            unset($this->limbo[$key]);
        }
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function getMissing()
    {
        if (count($this->limbo) == 0) {
            return null;
        }
        return head($this->limbo);
    }

    public function getLimbo()
    {
        return $this->limbo;
    }
}