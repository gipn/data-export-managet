<?php

namespace App\Lib\Tree;

class Tree
{
    protected $children = [];
    protected $data = [];

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function setProperties($properties)
    {
        $this->data = $properties;
    }

    public function addProperty($key, $value)
    {
        $this->data[$key] = $value;
    }

    /** @deprecated */
    public function getProperty($name, $default = false)
    {
        return $this->get($name, $default);
    }

    public function get($name, $default = false) 
    {
        return $this->data[$name] ?? $default;
    }

    /** @deprecated */
    public function hasProperty($name)
    {
        return $this->has($name);
    }

    public function has($name)
    {
        return isset($this->data[$name]);
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getChild($where, $value)
    {
        foreach ($this->children as $child) {
            if (!iiset($child[$where]) || $child[$where] != $value) {
                continue;
            }
            return $child;
        }
        return null;
    }

    public function addChild(Tree $child): Tree
    {
        $this->children[] = $child;
        return $this;
    }

    public function createChildrenWhere($data, $where, $value)
    {
        $nodes = [];
        foreach ($data as $d) {
            $nodes[] = new Tree($d);
        }
        return $this->addChildrenWhere($nodes, $where, $value);
    }

    public function addChildrenWhere($nodes, $where, $value)
    {
        $added = [];
        foreach ($nodes as $node) {
            if (!$node->has($where) || $node->get($where) != $value) {
                continue;
            }
            $added[] = $node;
            $this->addChild($node);
        }
        return $added;
    }

    public function hasChild()
    {
        return (count($this->children) > 0);
    }
}
