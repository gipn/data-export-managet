<?php

namespace App\Lib\Transformation;

class Prototype
{
    protected $iterations = [];
    protected $result = [];

    public function fill($key, $value)
    {
        if (strpos($key, "$") !== false) {
            if (!isset($this->iterations[$key])) {
                $this->iterations[$key] = 0;
            }
            $i = $this->iterations[$key];
            $this->iterations[$key]++;
            $key = \str_replace("$", $i, $key);
        }
        array_set($this->result, $key, $value);
    }

    public function toArray()
    {
        return $this->result;
    }
}