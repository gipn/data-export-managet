<?php

namespace App\Lib\Export;

use App\Lib\Tree\Tree;

class ExportTree 
{
    /** @var Tree */
    protected $tree;
    /** @var DataStack */
    protected $dataStack;

    public function __construct(Tree $tree, $dataStack = null)
    {
        $this->tree = $tree;
        $this->dataStack = $dataStack;
        if ($this->dataStack == null) {
            $this->dataStack = new DataStack();
        }
    }

    public function forItem($item)
    {
        $this->dataStack->push($item);
        return $this;
    }

    public function forItems($items)
    {
        $this->dataStack->pushAll(\array_reverse($items));
        return $this;
    }

    public function toArray()
    {
        $result = [];
        
        while (($dataItem = $this->dataStack->pop()) != null) {
            $result[] = $this->applyItem($this->tree, $dataItem);
        }
        if (count($result) == 1) {
            $result = $result[0];
        }
        return $result;
    }

    protected function applyItem($treeNode, $item) 
    {
        if (!$this->equals($treeNode) && $treeNode->hasProperty('behavior') && $treeNode->getProperty('behavior') == ">repeat") {
            $this->dataStack->push($item);
            $subTree = new ExportTree($treeNode, $this->dataStack);
            return $subTree->toArray();
        } else if ($treeNode->hasProperty('command')) {
            $result = $this->runCommand($treeNode->getProperty('command'), $item);    
        } else {
            $result = [];
            foreach ($treeNode->getChildren() as $child) {
                $result = \array_merge($result, $this->applyItem($child, $item));
            }
        }
        return [
            $treeNode->getProperty('name') => $result
        ];
    }

    protected function runCommand($command, $data)
    {
        if (!\is_string($command) || \strlen($command) == 0 || $command[0] != ":") {
            return $command;
        }
        $dataKey = \substr($command, 1);
        return $data->getValue($dataKey);
    }

    protected function equals($treeNode)
    {
        return $this->tree === $treeNode;
    }
}