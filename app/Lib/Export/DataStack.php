<?php

namespace App\Lib\Export;

class DataStack
{
    protected $array;

    public function __construct($array = [])
    {
        $this->array = $array;
    }

    public function pop()
    {
        if (count($this->array) == 0) {
            return null;
        }
        return \array_pop($this->array);
    }

    public function push($item)
    {
        $this->array[] = $item;
    }

    public function pushAll($items)
    {
        $this->array = \array_merge($this->array, $items);
    }
}