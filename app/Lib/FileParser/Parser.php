<?php

namespace App\Lib\FileParser;

class Parser
{
    public function __construct()
    {
        
    }

    public function fromText($text)
    {
        return $text;
    }

    public function fromFile($fileName)
    {
        $content = \file_get_contents($fileName);
        return $this->fromText($content);
    }
}