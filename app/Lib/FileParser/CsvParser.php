<?php 

namespace App\Lib\FileParser;

class CsvParser extends Parser
{
    public function fromText($text)
    {
        $parsedData = [];
        $rows = explode(PHP_EOL, $text);
        $header = \array_shift($rows);
        $header = \str_getcsv($header);
        foreach ($rows as $rowNumber => $row) {
            if (empty($row)) {
                continue;
            }
            $row = \str_getcsv($row);
            $data = [];
            foreach ($row as $columnNumber => $value) {
                $data[$header[$columnNumber]] = $value;
            }
            $parsedData[$rowNumber] = $data;
        }

        return $parsedData;
    }
}