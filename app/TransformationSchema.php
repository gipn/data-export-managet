<?php

namespace App;

use App\Properties;

class TransformationSchema extends PropertiesModel
{
    protected $table = 'transformation_schema';

    public function transformation() 
    {
        return $this->belongsTo(Transformation::class);
    }

    public function getName()
    {
        $parts = explode(".", $this->output);
        return last($parts);
    }

    public function selectChildren($schemas)
    {
        $self = $this;
        return $schemas->filter(function ($item, $key) use ($self) {
            $item->parent_id == $self->id;
        });
    }

    public function hasParent()
    {
        return $this->parent_id > 0;
    }

    public function getParentKey()
    {
        $parts = explode(".", $this->output);
        array_pop($parts);
        if (count($parts) == 0) {
            return false;
        }
        return implode(".", $parts);
    }
}
