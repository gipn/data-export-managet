<?php

namespace App;

use App\Properties;
use Illuminate\Database\Eloquent\Model;

class Transformation extends Model
{
    protected $table = 'transformation';

    public function transformationSchema() 
    {
        return $this->hasMany(TransformationSchema::class);
    }

    public function getSchemaCountAttribute() 
    {
        return $this->transformationSchema()->count();
    }

    public function properties()
    {
        return $this->hasMany(Properties::class, 'foreign_id')->where('namespace', 'transformation');
    }

    public function repeatBy()
    {
        foreach ($this->transformationSchema as $schema) {
            if ($schema->behavior != "repeat") {
                continue;
            }
            return $schema->id;
        }
        return 0;
    }
}
