<?php

namespace App;

class ExportBinding extends PropertiesModel
{
    protected $table = "export_binding";

    public function export() 
    {
        return $this->belongsTo(Export::class);
    }

    public function transformationSchema() 
    {
        return $this->belongsTo(TransformationSchema::class);
    }

    public function getKey()
    {
        $key = $this->transformationSchema->output;
        if (strpos($key, "$") !== false) {
            $key = \str_replace("$", "[" . $this->getPropertyValue('$id', "") . "]", $key);
        }
        return $key;
    }
}
