<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    const STATE_IDLE = 1;
    const STATE_RUNNING = 2;

    protected $table = "export";

    public function transformation() 
    {
        return $this->belongsTo(Transformation::class);
    }

    public function dataGroup() 
    {
        return $this->belongsTo(DataGroup::class);
    }

    public function bindings() 
    {
        return $this->hasMany(ExportBinding::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
