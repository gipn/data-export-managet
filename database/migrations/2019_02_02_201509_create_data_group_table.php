<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data_group')) {
            return;
        }
        Schema::create('data_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('name', 255);
            $table->timestamps();

            $table->index(['customer_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_group');
    }
}
