<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransformationSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('transformation_schema')) {
            return;
        }
        Schema::create('transformation_schema', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transformation_id');
            $table->integer('parent_id');
            $table->string('output', 255);
            $table->text('behavior');
            $table->text('description');
            $table->boolean('required');
            $table->timestamps();

            $table->index(['transformation_id', 'parent_id', 'output']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transformation_schema');
    }
}
