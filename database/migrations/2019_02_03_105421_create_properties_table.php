<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('properties')) {
            return;
        }
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('namespace', 255);
            $table->integer('foreign_id');
            $table->string('name', 255);
            $table->text('value');
            $table->timestamps();

            $table->index(['namespace', 'foreign_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
