<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportBindingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('export_binding')) {
            return;
        }
        Schema::create('export_binding', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('export_id');
            $table->integer('transformation_schema_id');
            $table->text('command');
            $table->timestamps();

            $table->index(['export_id', 'transformation_schema_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_binding');
    }
}
