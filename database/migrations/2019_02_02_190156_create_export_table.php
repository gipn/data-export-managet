<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('export')) {
            return;
        }
        Schema::create('export', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('transformation_id');
            $table->integer('data_group_id');
            $table->timestamp('activated_at');
            $table->text('file_name', 255);
            $table->text('extension', 20);
            $table->smallInteger('state');
            $table->timestamps();

            $table->index(['customer_id', 'data_group_id']);
            $table->index(['customer_id', 'activated_at']);
            $table->index(['customer_id', 'file_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export');
    }
}
