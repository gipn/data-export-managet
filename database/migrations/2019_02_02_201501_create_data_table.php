<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data')) {
            return;
        }
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_internal_id', 255);
            $table->integer('data_group_id');
            $table->longText('value');
            $table->timestamps();

            $table->index(['customer_internal_id', 'data_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
