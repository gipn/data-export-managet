<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/transformation', 'TransformationController@index')
    ->name('transformation.index');
Route::get('/transformation/create', 'TransformationController@create')
    ->name('transformation.create');
Route::post('/transformation/store', 'TransformationController@store')
    ->name('transformation.store');

Route::get('/transformation/{transformationId}/schema/{id}/edit', 'TransformationSchemaController@edit')
    ->name('transformation_schema.edit');
Route::post('/transformation/{transformationId}/schema/{id}/update', 'TransformationSchemaController@update')
    ->name('transformation_schema.update');
Route::get('/transformation/{transformationId}/schema/create', 'TransformationSchemaController@create')
    ->name('transformation_schema.create');
Route::post('/transformation/{transformationId}/schema/store', 'TransformationSchemaController@store')
    ->name('transformation_schema.store');
Route::get('/transformation/{id}', 'TransformationController@show')
    ->name('transformation.show');
Route::get('/transformation/{id}/mark-repeat-at/{schemaId}', 'TransformationController@markRepeatAt')
    ->name('transformation.markRepeatAt');

Route::get('/export', 'ExportController@index')
    ->name('export.index');
Route::get('/export/create', 'ExportController@create')
    ->name('export.create');
Route::post('/export/store', 'ExportController@store')
    ->name('export.store');
Route::get('/export/{id}', 'ExportController@show')
    ->name('export.show');
Route::get('/export/{id}/run', 'ExportController@run')
    ->name('export.run');
Route::get('/export/{id}/preview', 'ExportController@preview')
    ->name('export.preview');

Route::get('/export/{exportId}/binding/{id}/edit', 'ExportBindingController@edit')
    ->name('export_binding.edit');
Route::post('/export/{exportId}/binding/{id}update', 'ExportBindingController@update')
    ->name('export_binding.update');
Route::get('/export/{exportId}/binding/create', 'ExportBindingController@create')
    ->name('export_binding.create');
Route::post('/export/{exportId}/binding/store', 'ExportBindingController@store')
    ->name('export_binding.store');

Route::get('/data', 'DataGroupController@index')
    ->name('data_group.index');
Route::get('/data/create', 'DataGroupController@create')
    ->name('data_group.create');
Route::post('/data/store', 'DataGroupController@store')
    ->name('data_group.store');
Route::post('/data/{id}/properties', 'DataGroupController@properties')
    ->name('data_group.properties');
Route::get('/data/{id}', 'DataGroupController@show')
    ->name('data_group.show');

Route::get('/data/{id}/import/set', 'DataImportController@set')
    ->name('data_import.set');
Route::get('/data/{id}/import/add', 'DataImportController@add')
    ->name('data_import.add');

Route::get('/file/{publicId}', 'FileController@download')
    ->name('file.download');

Route::get('/', function () {
    return view('welcome');
});
