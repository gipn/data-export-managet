@extends('layout.default')

@section('title', 'Prepojenie na')

@section('content')
    <h1>Prepojenie na</h1>
    @form([
        'action' => $onSubmit
    ])
        @inputhidden([
            'name' => 'transformation_schema_id',
            'value' => $values['transformation_schema_id'] ?? 0
        ])@endinputhidden
        @inputtext([
            'label' => $schema->output . " = ",
            'name' => 'command',
            'value' => $values['command'] ?? ""
        ])@endinputtext

        @submit([
            'text' => 'Uloz'
        ])@endsubmit
    @endform    
@endsection
