@extends('layout.default')

@section('title', 'Data export manager')

@section('content')
    @table([
        'table' => $table,
        'name' => 'Zoznam',
        'onCreate' => route('transformation.create')
    ])@endtable    
@endsection

