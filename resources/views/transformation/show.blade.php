@extends('layout.default')

@section('title', $transformation->name)

@section('content')
    <h1>{{ $transformation->name }}</h1>
    @link([
        'if' => !empty($onAdd),
        'href' => $onAdd
    ])ADD
    @endlink
    @foreach ($tree->getChildren() as $node)
        @tree([
            'node' => $node,
            'level' => 0
        ])@endtree
    @endforeach
@endsection
