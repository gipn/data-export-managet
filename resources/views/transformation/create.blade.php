@extends('layout.default')

@section('title', 'Novy Export')

@section('content')
    <h1>Novy Export</h1>
    @form([
        'action' => route('transformation.store')
    ])
        @inputtext([
            'label' => 'nazov',
            'name' => 'name'
        ])@endinputtext

        @submit([
            'text' => 'Uloz'
        ])@endsubmit
    @endform    
@endsection
