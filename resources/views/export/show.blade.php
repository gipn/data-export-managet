@extends('layout.default')

@section('title', $export->dataGroup->name . "->" . $export->transformation->name)

@section('content')
    <h1>{{ $export->dataGroup->name }} -> {{ $export->transformation->name}}</h1>
    <div class="flexbox flexbox--row">
        <div class="block">
            <div class="block__title">Schema</div>
            @foreach ($schemaTree->getChildren() as $node)
                @tree([
                    'node' => $node,
                    'level' => 0
                ])@endtree
            @endforeach
        </div>
        <div class="block">
            <div class="block__title">Transformacia</div>
            @foreach ($bindings as $binding)
                <div>
                    <a href="{{ $binding['onClick'] }}">{{$binding['from'] }} -> {{ $binding['to'] }}</a>
                </div>
            @endforeach
        </div>
    </div>
    <div><a href="{{ route('export.run', ['id' => $export->id]) }}">RUN</a></div>
    <div><a href="{{ route('export.preview', ['id' => $export->id]) }}">PREVIEW</a></div>
    @if ($export->file->size > 0)
        <div><a href="{{ route('file.download', ['publicId' => $export->file->public_id]) }}">{{ route('file.download', ['publicId' => $export->file->public_id]) }}</a></div>
    @endif
@endsection
