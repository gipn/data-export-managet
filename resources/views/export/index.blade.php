@extends('layout.default')

@section('title', 'Moje Exporty')

@section('content')
    @table([
        'table' => $table,
        'name' => 'Moje Exporty',
        'onCreate' => route('export.create')
    ])@endtable    
@endsection

