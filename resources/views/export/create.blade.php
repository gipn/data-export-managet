@extends('layout.default')

@section('title', 'Novy Export')

@section('content')
    <h1>Novy Export</h1>
    @form([
        'action' => route('export.store')
    ])
        @inputselect([
            'label' => 'exportovat na',
            'name' => 'transformation_id',
            'options' => $transformations
        ])@endinputselect

        @inputselect([
            'label' => 'pouzit data',
            'name' => 'data_group_id',
            'options' => $dataGroups
        ])@endinputselect

        @inputselect([
            'label' => 'format vystupu',
            'name' => 'extension',
            'options' => [
                '.xml' => '.xml',
                '.csv' => '.csv'
            ]
        ])@endinputselect

        @inputtext([
            'label' => 'nazov suboru',
            'name' => 'filename'
        ])@endinputtext

        @inputcheckbox([
            'label' => 'aktivovat',
            'name' => 'activated'
        ])@endinputcheckbox

        @submit([
            'text' => 'Uloz'
        ])@endsubmit
    @endform    
@endsection
