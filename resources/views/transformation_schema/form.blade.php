@extends('layout.default')

@section('title', 'Novy Parameter')

@section('content')
    <h1>Novy Parameter</h1>
    @form([
        'action' => $onSubmit
    ])
        @inputhidden([
            'name' => 'parent',
            'value' => $values['parent'] ?? 0
        ])@endinputhidden

        @inputtext([
            'label' => 'nazov',
            'name' => 'output',
            'value' => $values['output'] ?? ""
        ])@endinputtext

        @inputtextarea([
            'label' => 'popis',
            'name' => 'description',
            'value' => $values['description'] ?? ""
        ])@endinputtextarea

        @inputtext([
            'label' => 'správanie',
            'name' => 'behavior',
            'value' => $values['behavior'] ?? ""
        ])@endinputtext

        @inputcheckbox([
            'label' => 'povinny',
            'name' => 'required',
            'checked' => $values['required'] ?? true
        ])@endinputtext

        @submit([
            'text' => 'Uloz'
        ])@endsubmit
    @endform    
@endsection
