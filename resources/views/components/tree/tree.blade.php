<div class="tree tree--level-{{ $level }}">
    <div>
        @link([
            'if' => $node->hasProperty('onClick'),
            'href' => $node->getProperty('onClick')
        ])
            <span class="bold">{{ $node->getProperty('name') }}</span>
            <span>{{ $node->getProperty('value', "") }}</span>
        @endlink
        @if ($node->hasProperty('onAdd'))
            @link([
                'href' => $node->getProperty('onAdd')
            ])+
            @endlink
        @endif
        @if ($node->hasProperty('note'))
            <span class="idle">{{ $node->getProperty('note') }}</span>
        @endif
    </div>
    @foreach ($node->getChildren() as $childreNode)
        @tree([
            'node' => $childreNode,
            'level' => $level + 1
        ])@endtree
    @endforeach
</div>