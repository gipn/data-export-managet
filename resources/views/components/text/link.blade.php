@if (!isset($if) || $if !== false)
    <a href="{{ $href }}">
@endif
{{ $slot }}
@if (!isset($if) || $if !== false)
    </a>
@endif