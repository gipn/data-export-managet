@if (empty($id))
    <?php $id = Illuminate\Support\Str::uuid(); ?>
@endif
<div class="">
    @if (!empty($label))
        <label for="{{ $id }}">{{ $label }}</label>
    @endif
    <select id="{{ $id }}" name="{{ $name }}">
        @foreach ($options as $key => $option)
            <option value="{{ $key }}" {{ ((isset($value) && $key == $value) ? 'selected' : '') }}>{{ $option }}</option>
        @endforeach
    </select>
</div>