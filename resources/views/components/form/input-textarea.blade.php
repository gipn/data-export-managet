@if (empty($id))
    <?php $id = Illuminate\Support\Str::uuid(); ?>
@endif
<div class="">
    @if (!empty($label))
        <label for="{{ $id }}">{{ $label }}</label>
    @endif
    <textarea id="{{ $id }}" name="{{ $name }}">{{ $value ?? '' }}</textarea>
</div>