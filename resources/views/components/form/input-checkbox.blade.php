@if (empty($id))
    <?php $id = Illuminate\Support\Str::uuid(); ?>
@endif
<div class="">
    @if (!empty($label))
        <label for="{{ $id }}">{{ $label }}</label>
    @endif
<input id="{{ $id }}" type="checkbox" name="{{ $name }}" value="1" {{ ((isset($checked) && $checked == true) ? 'checked' : '') }}>
</div>