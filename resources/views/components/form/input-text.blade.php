@if (empty($id))
    <?php $id = Illuminate\Support\Str::uuid(); ?>
@endif
<div class="">
    @if (!empty($label))
        <label for="{{ $id }}">{{ $label }}</label>
    @endif
    <input id="{{ $id }}" type="text" name="{{ $name }}" value="{{ $value ?? '' }}">
</div>