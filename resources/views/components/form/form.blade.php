<form class="{{ $classes ?? "" }}" action="{{ $action }}" method="{{ $method ?? 'POST' }}">
    @if (!isset($csrf) || $csrf !== false)
        @csrf
    @endif
    {{ $slot }}
</form>