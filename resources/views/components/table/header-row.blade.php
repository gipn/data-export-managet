<div class="table__row table__row--header flexbox flexbox--row">
    @foreach ($schema as $cell)
        <?php $align = $cell['align'] ?? 'right'; ?>
        <?php $flex = (!isset($cell['flex']) || $cell['flex'] !== false) ?>
        <div class="table__cell table__cell--header {{ ($flex ? 'flex' : '') }} text-{{ $align }}">{{ $cell['name'] }}</div>
    @endforeach
</div>