<div class="table__header flexbox flexbox--row">
    <div class="table__header-name flex">{{ $name }}</div>
    <div class="table__header-actions">
        @if (!empty($onCreate))
            <a class="link link--transparent" href="{{ $onCreate }}"><span title="vytvor">C</span></a>
        @endif
    </div>
</div>