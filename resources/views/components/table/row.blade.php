

<div class="table__row table__row--hover flexbox flexbox--row">
    @if ($row->hasProperty('onClick'))
        <a class="link link--transparent flexbox flexbox--row flex" href="{{ $row->getProperty('onClick') }}">
    @endif
    @foreach ($row->getCells() as $cell)
        <?php $align = $cell['schema']['align'] ?? 'right'; ?>
        <?php $flex = (!isset($cell['schema']['flex']) || $cell['schema']['flex'] !== false) ?>
        <div class="table__cell {{ ($flex ? 'flex' : '') }} text-{{ $align }}">{{ $cell['value'] }}</div>
    @endforeach
    @if ($row->hasProperty('onClick'))
        </a>
    @endif
    @if ($row->hasProperty('onEdit'))
        <div class="table__cell"><a href="{{ $row->getProperty('onEdit') }}">E</a></div>
    @endif
</div>
