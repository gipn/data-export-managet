<div class="table flexbox flexbox--column">
    @tableheader([
        'name' => $name,
        'onCreate' => $onCreate ?? null
    ])@endtableheader
    @tableheaderrow([
        'schema' => $table->getSchema()
    ])@endtableheaderrow
    @foreach ($table->getRows() as $row)
        @tablerow([
            'row' => $row
        ])@endtablerow
    @endforeach
</div>