@extends('layout.default')

@section('title', 'Moje Data')

@section('content')
    @table([
        'table' => $table,
        'name' => 'Moje Data',
        'onCreate' => route('data_group.create')
    ])@endtable    
@endsection

