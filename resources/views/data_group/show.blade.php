@extends('layout.default')

@section('title', $dataGroup->name)

@section('content')
<h1>{{ $dataGroup->name }}</h1>
    @form([
        'action' => route('data_group.properties', ['id' => $dataGroup->id])
    ])
        @inputtext([
            'label' => 'url',
            'name' => 'url',
            'value' => $properties['url'] ?? ''
        ])@endinputtext

        @submit([
            'text' => 'Uloz'
        ])@endsubmit
    @endform
<div><a href="{{ route('data_import.add', ['id' => $dataGroup->id]) }}">ADD</a></div>
<div><a href="{{ route('data_import.set', ['id' => $dataGroup->id]) }}">SET</a></div>
@endsection
